#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 19 15:00:30 2023

@author: Youwan Mahé
"""
import numpy as np
from matplotlib import pyplot as plt 

def create_board(n :int , m:int ):
    board = np.zeros((n,m))
    return board

def fit_stones(board, column):
    NumberofPlayer1Stones = np.count_nonzero(board == 1)
    NumberofPlayer2Stones = np.count_nonzero(board == 2)
    
        
    if(NumberofPlayer1Stones<=NumberofPlayer2Stones):
        whoseTurn = 1
        
    else:
        whoseTurn=2        
    
    for i in range(0,board.shape[0]):
        if (board[board.shape[1]-i][column] == 0):
            board[board.shape[1]-i][column] = whoseTurn
            break

    return board ,board.shape[1]-i, column

def draw(board):
    
    plt.title("Connect 4") 
    
    n=board.shape[0]
    m=board.shape[1]
    
    for row in range(0,n):
        for col in range(0,m):
            if board[row][col]==1:
                print("Y", end=" ")
                plt.plot(col,n-row,"oy",markersize=20)
            elif board[row][col]==2:
                print("R", end=" ")
                plt.plot(col,n-row,"or",markersize=20)
            else:
                print(" ", end=" ")
        print("")
    plt.xlim([-1, m+1])
    plt.ylim([0, n+1])
    plt.show()
    
def test(board,x,y):
    
    n=board.shape[0]
    m=board.shape[1]
    
    color = board[x][y]
    
    #test horizontal
    
    horizontal = 0
    for stone in board[x]:
        if stone == color:
            horizontal = horizontal + 1 
        else :
            horizontal = 0
        if horizontal == 4:
            return 1
        
    #test vertical
        
    vertical = 0
    for stone in board[:,y]:
        if stone == color:
            vertical = vertical + 1 
        else :
            vertical = 0
        if vertical == 4:
            return 1

    #test diag
        
    diag = 0
    
    for i in range(0,4):
        try :
            if(board[x+i][y+i]==color):
                diag=diag+1
            else:
                diag=0
            if diag ==4 : 
                return 1
        except :
            break
    diag = 0
        
    for i in range(0,4):
        try :
            if(board[x-i][y+i]==color):
                diag=diag+1
            else:
                diag=0
            if diag==4:
                return 1
        except :
            break
    diag = 0
        
    for i in range(0,4):
        try :
            if(board[x+i][y-i]==color):
                diag=diag+1
            else:
                diag=0
            if diag == 4:
                return 1
        except :
            break
    diag = 0
        
    for i in range(0,4):
        try :
            if(board[x-i][y-i]==color):
                diag=diag+1
            else:
                diag=0
            if diag==4:
                return 1
        except :
            break
        
    return 0

      
