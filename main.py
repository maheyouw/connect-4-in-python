#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 19 14:56:14 2023

@author: Youwan Mahé
"""

from connect import *

def main() -> None:

    board = create_board(7,6)
    
    col = 0
    win = 0

    while(win != 1):
        
        try:
            col = int(input("Choose a column = "))
        except ValueError:
            print("You must enter a number between 0 and 5")
            continue
        if (col<0) or (col > 5):
            print("Column number must be between 0 and 5")
            continue
        board,x,y =fit_stones(board, col)
        draw(board)
        win=test(board,x,y)
    print("Game Over")

if __name__ == '__main__':
    main()
